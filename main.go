package main

import (
	"encoding/json"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	tb "gopkg.in/tucnak/telebot.v2"
)

var reminders = map[int]int{}
var nextReminder = map[int]int64{}
var stickers = map[string]*tb.Sticker{}
var b *tb.Bot

func main() {

	readReminders()
	readStickers()
	calculateNext()
	var err error
	b, err = tb.NewBot(tb.Settings{
		Token:  os.Getenv("API_KEY"),
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	b.Handle("/start", func(m *tb.Message) {
		b.Send(m.Sender, stickers["hi"])
		b.Send(m.Sender, "Приветствую тебя, Юзер!\n\n"+
			" Если ты хочешь шобы я тебе присылал напоминашки, напиши мне \"/coal 10\", "+
			"и я тебе буду присылать напоминашки раз в 10 минут, если напишешь\"/coal 15\" "+
			"- раз в 15 минут, ну ты поял.\n\nКогда я тебя заебу - напиши \"/harosh\" и напоминашки остановятся")

	})
	b.Handle("/coal", func(m *tb.Message) {
		parts := strings.Split(m.Text, " ")
		if len(parts) < 2 {
			b.Send(m.Sender, "Вот так нада: \"/coal N\" - где N - кол-во минут")
			return
		}

		minutes, err := strconv.Atoi(parts[1])
		if err != nil {
			b.Send(m.Sender, "Чота "+parts[1]+" не похоже на число")
			return
		}
		reminders[m.Sender.ID] = minutes
		nextReminder[m.Sender.ID] = getNextReminderTime(minutes)
		saveReminders()
		b.Send(m.Sender, "👌")

	})
	b.Handle("/harosh", func(m *tb.Message) {

		delete(nextReminder, m.Sender.ID)
		delete(reminders, m.Sender.ID)
		saveReminders()

		b.Send(m.Sender, stickers["stop"])
	})

	go runReminder()
	b.Start()
}

func saveReminders() {
	s := ""
	for uid, minute := range reminders {
		s += strconv.Itoa(uid)
		s += ":"
		s += strconv.Itoa(minute)
		s += "\n"
	}
	err := os.WriteFile("reminders.txt", []byte(s), 0644)
	if err != nil {
		log.Fatalln(err)
	}
}

func runReminder() {
	for {
		utc := time.Now().UTC().Unix()

		for uid, target := range nextReminder {
			if target <= utc {
				b.Send(&tb.User{ID: uid}, "Пора угольнуть!")
				nextReminder[uid] = getNextReminderTime(reminders[uid])
			}
		}
		time.Sleep(10 * time.Second)

	}
}

func calculateNext() {
	for userId, minutes := range reminders {
		nextReminder[userId] = getNextReminderTime(minutes)
	}
}

func getNextReminderTime(minutes int) int64 {
	utc := time.Now().UTC().Unix()
	return utc + int64(60)*int64(minutes)
}

func readStickers() {
	content, err := os.ReadFile("stickers.json")
	if err != nil {
		log.Fatalln(err.Error())
	}
	err = json.Unmarshal(content, &stickers)
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func readReminders() {
	content, err := os.ReadFile("reminders.txt")
	if err != nil {
		if os.IsNotExist(err) {
			return
		}
		log.Fatalln(err.Error())
	}

	for _, line := range strings.Split(string(content), "\n") {
		parts := strings.Split(line, ":")
		uid, _ := strconv.Atoi(parts[0])
		minutes, _ := strconv.Atoi(parts[1])
		reminders[uid] = minutes
	}
}
